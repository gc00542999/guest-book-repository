/**
 * 
 */
package com.guestbook.service;

import static org.mockito.Mockito.when;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

import com.guestbook.constants.DataProtectionKeyType;
import com.guestbook.constants.GuestBookConstants;
import com.guestbook.dao.DataProtectionKeyDao;
import com.guestbook.domain.DataProtectionKey;
import com.guestbook.domain.UserAccount;
import com.guestbook.util.EncryptionUtil;
import com.guestbook.vo.LoginVo;

/**
 * @author GC00542999
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class GuestBookAuthServiceImplTest {

	@InjectMocks
	GuestBookAuthServiceImpl guestBookAuthServiceImpl;

	@Spy
	LoginVo login;

	@Mock
	MockHttpServletRequest request;
	@Mock
	DataProtectionKeyDao dataProtectionKeyDao;

	@Spy
	DataProtectionKey dataProtectionKey;
	
	@Mock
	EncryptionUtil encryptionUtil;
	MockHttpSession session;
	Method getRoleMethod = null;

	@Mock
	private Principal principal;

	@Mock
	private SecurityContext securitycontext;

	@Mock
	private Authentication authentication;
	@Mock
	AuthenticationManager authenticationManager;
	@Mock
	UsernamePasswordAuthenticationToken authenticationToken;

	Method getSaltMethod;

	/**
	 * @throws SecurityException
	 * @throws NoSuchMethodException
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws NoSuchMethodException {

		getSaltMethod = guestBookAuthServiceImpl.getClass().getDeclaredMethod("getSalt");
		getSaltMethod.setAccessible(true);
		getRoleMethod = guestBookAuthServiceImpl.getClass().getDeclaredMethod("getRole", Authentication.class);
		getRoleMethod.setAccessible(true);

	}

	/**
	 * Test method for
	 * {@link com.guestbook.service.GuestBookAuthServiceImpl#authenticateAndStartSession(com.guestbook.vo.LoginVo, javax.servlet.http.HttpServletRequest)}.
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 * @throws InvalidAlgorithmParameterException 
	 * @throws NoSuchPaddingException 
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidKeyException 
	 * 
	 * @throws InvocationTargetException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	@Test(expected = Exception.class)
	public void testAuthenticateAndStartSession() throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
		login.setPassword("guest1");
		dataProtectionKey.setValue("value");
		UserAccount user = new UserAccount();
		when(authenticationManager.authenticate(authenticationToken)).thenReturn(authentication);
		when(authentication.getPrincipal()).thenReturn(user);
		when(securitycontext.getAuthentication()).thenReturn(authentication);
		SecurityContextHolder.setContext(securitycontext);
		when(dataProtectionKeyDao.findByNameAndType(GuestBookConstants.SALT_LOGIN, DataProtectionKeyType.SALT.name()))
				.thenReturn(dataProtectionKey);
		when(encryptionUtil.decrypt(dataProtectionKey.getValue(), encryptionUtil.fetchSecretKeyFromValt()))
				.thenReturn("salt1");

		when(request.getSession(true)).thenReturn(session);
		
		guestBookAuthServiceImpl.authenticateAndStartSession(login, request);

	}

	@Test(expected = Test.None.class)
	public void testAuthenticateAndStartSessionNull() throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
		guestBookAuthServiceImpl.authenticateAndStartSession(null, request);
	}

	/**
	 * Test method for
	 * {@link com.guestbook.service.GuestBookAuthServiceImpl#getUsername()}.
	 */
	@Test(expected = Test.None.class)
	public void testGetUsername() {
		UserAccount user = new UserAccount();
			
		when(authenticationManager.authenticate(authenticationToken)).thenReturn(authentication);
		when(authentication.getPrincipal()).thenReturn(user);
		when(securitycontext.getAuthentication()).thenReturn(authentication);
		SecurityContextHolder.setContext(securitycontext);
		guestBookAuthServiceImpl.getUsername();
	}
	
	@Test(expected = Test.None.class)
	public void testGetUsernameDetails() {
		 User.UserBuilder users = User.builder();
			UserDetails userDetails =users.username("username").password("Password").roles("ADMIN").build();
			
			
		when(authenticationManager.authenticate(authenticationToken)).thenReturn(authentication);
		when(authentication.getPrincipal()).thenReturn(userDetails);
		when(securitycontext.getAuthentication()).thenReturn(authentication);
		SecurityContextHolder.setContext(securitycontext);
		guestBookAuthServiceImpl.getUsername();
	}

	@Test(expected = Test.None.class)
	public void testGetRole() throws IllegalAccessException, InvocationTargetException {
		User.UserBuilder users = User.builder();
		UserDetails userDetails = users.username("username").password("Password").roles("ADMIN").build();
		when(authentication.getPrincipal()).thenReturn(userDetails);

		getRoleMethod.invoke(guestBookAuthServiceImpl, authentication);
	}

}
