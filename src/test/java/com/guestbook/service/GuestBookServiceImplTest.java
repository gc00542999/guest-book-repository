package com.guestbook.service;

import static com.guestbook.constants.GuestBookConstants.PAGE_LIMIT;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.multipart.MultipartFile;

import com.guestbook.constants.Status;
import com.guestbook.dao.GuestDao;
import com.guestbook.domain.Guest;
import com.guestbook.vo.GuestVo;

@RunWith(MockitoJUnitRunner.class)
public class GuestBookServiceImplTest {

	@InjectMocks
	GuestBookServiceImpl guestBookServiceImpl;

	@Mock
	public GuestDao guestDao;
	@Mock
	GuestBookAuthService guestBookAuthService;
	@Mock
	Guest guest;
	@Mock
	GuestVo guestVo;
	@Mock
	List<Guest> guests;
	@Mock
	List<GuestVo> guestVos;
	
	@Mock
	MultipartFile file;

	
	@Test(expected=Test.None.class)
	public void testAddGuest() throws IllegalAccessException, InvocationTargetException {
		when(guestDao.save(guest)).thenReturn(guest);
		 guestBookServiceImpl.addGuest(guestVo);
		
	}

	@Test(expected=Test.None.class)
	public void testUpdateGuests() {
		List<GuestVo> guestVoList= Mockito.spy(new ArrayList<>());
		guestVoList.add(guestVo);
		when(guestDao.saveAll(guests)).thenReturn(guests);
		guestBookServiceImpl.updateGuests(guestVoList);
		

	}

	
	@Test(expected=Test.None.class)
	public void testGetAllNewGuestEntriesIntStatus() {
		Pageable pageable = PageRequest.of(1, 2, Sort.by("id"));
		when(guestDao.findByStatus(Status.NEW.name(), pageable)).thenReturn(guests);
		guestBookServiceImpl.getAllNewGuestEntries(1, Status.NEW);
	}

	@Test(expected = NullPointerException.class)
	public void testGetImageById() {

		guestBookServiceImpl.getImageById(1l);
	}

	@Test(expected=Test.None.class)
	public void testAddGuestWithImage() {
		byte[] bytes = new byte[10];
		try {
			when(file.getBytes()).thenReturn(bytes);
		} catch (IOException e1) {
			
			assertTrue("addGuestWithImage failed", false);
		}
		when(file.getOriginalFilename()).thenReturn("details.png");
		when(guestDao.save(guest)).thenReturn(guest);
		try {
			guestBookServiceImpl.addGuestWithImage(file, "username");
		} catch (IOException | SQLException e) {
			
			assertTrue("addGuestWithImage failed", false);
		}
	}

	@Test(expected=Test.None.class)
	public void testGetTotalGuestPages() {
		when(guestDao.countByStatus(Status.NEW.name())).thenReturn(1l);
		guestBookServiceImpl.getTotalGuestPages(Status.NEW);
	}

	@Test(expected=Test.None.class)
	public void testGetDefaultGuestListVo() {
		List<Guest> guestList= Mockito.spy(new ArrayList<>());
		guestList.add(guest);
		Pageable pageable = PageRequest.of(0, PAGE_LIMIT, Sort.by("id"));
		when( guestDao.findByStatus(Status.NEW.name(), pageable)).thenReturn(guestList);
		when(guestDao.countByStatus(Status.NEW.name())).thenReturn(1l);
		when(guestBookAuthService.getUsername()).thenReturn("username");
		guestBookServiceImpl.getDefaultGuestListVo();
	}

}
