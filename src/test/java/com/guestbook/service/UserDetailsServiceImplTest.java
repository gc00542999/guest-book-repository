/**
 * 
 */
package com.guestbook.service;

import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.guestbook.dao.UserDao;
import com.guestbook.domain.UserAccount;

/**
 * @author GC00542999
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class UserDetailsServiceImplTest {

	
	@InjectMocks
	UserDetailsServiceImpl userDetailsServiceImpl;
	@Mock
	UserDao  userDao;
	@Mock
	UserAccount userAccount;
	
	
	/**
	 * Test method for {@link com.guestbook.service.UserDetailsServiceImpl#loadUserByUsername(java.lang.String)}.
	 */
	@Test(expected=Test.None.class)
	public void testLoadUserByUsername() {
		when(userDao.findByUsername("username")).thenReturn(userAccount);
		when(userAccount.getPassword()).thenReturn("password");
		when(userAccount.getRole()).thenReturn("guest");
		userDetailsServiceImpl.loadUserByUsername("username");
		
	}
	@Test(expected=UsernameNotFoundException.class)
	public void testLoadUserByUsernameNull() {
		when(userDao.findByUsername("username")).thenReturn(null);
		
		userDetailsServiceImpl.loadUserByUsername("username");
		
	}

}
