package com.guestbook.domain;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.guestbook.constants.UserRole;

public class UserAccountTest {
	
	UserAccount userAccount;
	

	@Before
	public void setUp() {
		userAccount =new UserAccount();
	}

	@Test
	public void testGetUsername() {
		String expected = "expectedUser";
		userAccount.setUsername(expected);
		assertEquals(expected, userAccount.getUsername());
	}

	@Test
	public void testGetPassword() {
		String expected = "expectedPswd";
		userAccount.setPassword(expected);
		assertEquals(expected, userAccount.getPassword());

	}

	@Test
	public void testGetRole() {
		String expected = "ADMIN";
		userAccount.setRole(expected);
		assertEquals(expected, userAccount.getRole());
	}

	@Test
	public void testGetFirstname() {
		String expected = "expectedFirstName";
		userAccount.setFirstname(expected);
		assertEquals(expected, userAccount.getFirstname());
	}

	@Test
	public void testGetLastname() {
		String expected = "expectedLastName";
		userAccount.setLastname(expected);
		assertEquals(expected, userAccount.getLastname());
	}

	@Test
	public void testGetEmail() {
		String expected = "expectedEmail";
		userAccount.setEmail(expected);
		assertEquals(expected, userAccount.getEmail());
	}

	@Test
	public void testGetAddress() {
		String expected = "expectedAddress";
		userAccount.setAddress(expected);
		assertEquals(expected, userAccount.getAddress());
	}

	@Test
	public void testGetPhone() {
		String expected = "123131232332";
		userAccount.setPhone(expected);
		assertEquals(expected, userAccount.getPhone());
	}

}
