package com.guestbook.domain;

import static org.junit.Assert.*;

import java.sql.Blob;

import org.junit.Before;
import org.junit.Test;



public class GuestTest {

	Guest guest;

	@Before
	public void setUp() {
		guest = new Guest();
	}

	@Test
	public void testGetSetId() {
		Long expectedValue = 1l;
		guest.setId(expectedValue);
		assertEquals(expectedValue, guest.getId());
	}

	@Test
	public void testGetSetNote() {
		String expectedValue = "expectedvalue";
		guest.setNote(expectedValue);
		assertEquals(expectedValue, guest.getNote());
	}

	@Test
	public void testGetSetImage() {
		Blob expectedValue =null;
		guest.setImage(expectedValue);
		assertEquals(expectedValue, guest.getImage());
	}

	@Test
	public void testGetSetUsername() {
		String username = "testUsername";
		guest.setUsername(username);
		assertEquals(username, guest.getUsername());
	}

	@Test
	public void testGetSetStatus() {
		String expectedValue = "expectedvalue";
		guest.setStatus(expectedValue);
		assertEquals(expectedValue, guest.getStatus());
	}

	@Test
	public void testGetSetType() {
		String expectedValue = "expectedvalue";
		guest.setType(expectedValue);
		assertEquals(expectedValue, guest.getType());
	}

}
