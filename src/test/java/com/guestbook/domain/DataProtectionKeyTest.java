package com.guestbook.domain;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class DataProtectionKeyTest {
	
	DataProtectionKey dataProtectionKey;

	@Before
	public void setUp() {
		dataProtectionKey = new DataProtectionKey();
	}

	@Test
	public void testGetSetId() {
		Long expectedValue=1l;
		dataProtectionKey.setId(expectedValue);
	assertEquals(expectedValue, dataProtectionKey.getId());
	}

	@Test
	public void testGetSetName() {
		String expectedValue="expectedValue";
		dataProtectionKey.setName(expectedValue);
	assertEquals(expectedValue, dataProtectionKey.getName());
	}

	@Test
	public void testGetSetType() {
		String expectedValue="expectedValue";
		dataProtectionKey.setType(expectedValue);
	assertEquals(expectedValue, dataProtectionKey.getType());
	}

	@Test
	public void testGetSetValue() {
		String expectedValue="expectedValue";
		dataProtectionKey.setValue(expectedValue);
	assertEquals(expectedValue, dataProtectionKey.getValue());
	}

}
