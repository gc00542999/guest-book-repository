package com.guestbook.vo;

import static org.junit.Assert.*;

import java.sql.Blob;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class GuestVoTest {

	GuestVo guestVo;

	@Before
	public void setUp() {
		guestVo = new GuestVo();
	}

	@Test
	public void testGetSetId() {
		Long expectedValue = 1l;
		guestVo.setId(expectedValue);
		assertEquals(expectedValue, guestVo.getId());
	}

	@Test
	public void testGetSetNote() {
		String expectedValue = "expectedvalue";
		guestVo.setNote(expectedValue);
		assertEquals(expectedValue, guestVo.getNote());
	}

	@Test
	public void testGetSetImage() {
		Blob expectedValue =null;
		guestVo.setImage(expectedValue);
		assertEquals(expectedValue, guestVo.getImage());
	}

	@Test
	public void testGetSetUsername() {
		String username = "testUsername";
		guestVo.setUsername(username);
		assertEquals(username, guestVo.getUsername());
	}

	@Test
	public void testGetSetStatus() {
		String expectedValue = "expectedvalue";
		guestVo.setStatus(expectedValue);
		assertEquals(expectedValue, guestVo.getStatus());
	}

	@Test
	public void testGetSetType() {
		String expectedValue = "expectedvalue";
		guestVo.setType(expectedValue);
		assertEquals(expectedValue, guestVo.getType());
	}

}
