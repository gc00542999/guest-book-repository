/**
 * 
 */
package com.guestbook.vo;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * Test class for LoginVo
 * @author gautamc
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class LoginVoTest {
	
	LoginVo loginVo ;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() {
		loginVo = new  LoginVo();
	}

	/**
	 * Test method for {@link com.guestbook.vo.LoginVo#getUsername()}.
	 */
	@Test
	public void testGetterSetterPassword() {
		String password="testpassword";
		loginVo.setPassword(password);
	assertEquals(password, loginVo.getPassword());
		
	}
	@Test
	public void testGetterSetterUsername() {
		String username="testusername";
		loginVo.setUsername(username);
	assertEquals(username, loginVo.getUsername());
		
	}

	

}
