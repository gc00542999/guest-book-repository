package com.guestbook.vo;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.guestbook.constants.Status;

public class GuestListVoTest {
	
	GuestListVo guestListVo;

	@Before
	public void setUp() {
		guestListVo = new GuestListVo();
	}

	

	@Test
	public void testGetSetGuests() {
		String expectedValue= "expectedValue";
		List<GuestVo> guests = new ArrayList<>();
		guests.add(new GuestVo());
		guests.get(0).setNote(expectedValue);
		guestListVo.setGuests(guests);
		assertEquals(expectedValue, guestListVo.getGuests().get(0).getNote());
	}

	@Test
	public void testGetSetUsername() {
		String expectedValue = "expectedvalue";
		guestListVo.setUsername(expectedValue);
		assertEquals(expectedValue, guestListVo.getUsername());
	}

	@Test
	public void testGetSetPageNo() {
		int expectedValue = 3;
		guestListVo.setPageNo(expectedValue);
		assertEquals(expectedValue, guestListVo.getPageNo());
	}

	@Test
	public void testGetSetStatus() {
		Status expectedValue =Status.APPROVED;
		guestListVo.setStatus(expectedValue);
		assertEquals(expectedValue, guestListVo.getStatus());
	}

	@Test
	public void testGetSetTotalPages() {
		int expectedValue = 3;
		guestListVo.setTotalPages(expectedValue);
		assertEquals(expectedValue, guestListVo.getTotalPages());
	}

}
