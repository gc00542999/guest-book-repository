package com.guestbook.controller;

import static com.guestbook.constants.GuestBookConstants.ADMIN_VIEW_PAGE;
import static com.guestbook.constants.GuestBookConstants.GUEST_LIST_FAIL_MSG;
import static com.guestbook.constants.GuestBookConstants.GUEST_LIST_SUCCESS_MSG;
import static com.guestbook.constants.GuestBookConstants.GUEST_SAVE_FAIL_MSG;
import static com.guestbook.constants.GuestBookConstants.GUEST_SAVE_SUCCESS_MSG;
import static com.guestbook.constants.GuestBookConstants.GUEST_VIEW_PAGE;
import static com.guestbook.constants.GuestBookConstants.UPLOAD_FAIL_MSG;
import static com.guestbook.constants.GuestBookConstants.UPLOAD_SUCCESS_MSG;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;

import java.io.File;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.Properties;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.ui.ModelMap;
import org.springframework.web.multipart.MultipartFile;

import com.guestbook.domain.Guest;
import com.guestbook.service.GuestBookAuthService;
import com.guestbook.service.GuestBookService;
import com.guestbook.vo.GuestVo;


@RunWith(MockitoJUnitRunner.class)
@WebAppConfiguration
public class GuestBookControllerTest {

	  private MockMvc mockMvc;
	
	@InjectMocks
	GuestBookController guestBookController;
	@Mock
	public GuestBookService guestBookService;
	@Mock
	public  GuestBookAuthService guestBookAuthService;
	@Mock
	MultipartFile file;
	@Mock
	File f;
	@Mock
	Blob blob;
//	@Autowired
	 @Spy
	 ResourceBundleMessageSource messageSource ;
	  @Mock
	  Guest guest;
	  @Mock
	  GuestVo guestVo;
	@Before
	public void setUp() {
		 MockitoAnnotations.initMocks(this);
		System.setProperty("log4j.configurationFile","log4j2-testConfig.xml");
		 this.mockMvc = MockMvcBuilders.standaloneSetup( guestBookController).build();
		 Properties commonMessages = new Properties();
			messageSource.setCommonMessages(commonMessages);
			
			commonMessages.setProperty(UPLOAD_SUCCESS_MSG ,"File uploaded successfully");
			commonMessages.setProperty(UPLOAD_FAIL_MSG ,"File upload failed");
			commonMessages.setProperty(GUEST_SAVE_SUCCESS_MSG , "Guest entry saved successfully");
			commonMessages.setProperty(GUEST_SAVE_FAIL_MSG ,"Guest entry could no be saved");
			commonMessages.setProperty(GUEST_LIST_SUCCESS_MSG,"Records updated successfully");
			commonMessages.setProperty(GUEST_LIST_FAIL_MSG , "Guest entry saved successfully");
			
			}

	@Test(expected=Test.None.class)
	public void testAddGuestEntry() throws Exception {
		
		when(guestBookService.addGuest(guestVo)).thenReturn(guest);
        mockMvc.perform(post("/addGuestEntry").requestAttr("guest", guest))
                 .andExpect(forwardedUrl("guestview"));
      
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected=Test.None.class)
	public void testAddGuestEntryException() throws Exception {
		
		when(guestBookService.addGuest(guestVo)).thenThrow(IllegalAccessException.class);
	//	  verify(guestBookService, times(1)).addGuest(guestVo);
        mockMvc.perform(post("/addGuestEntry").requestAttr("guest", guest))
                 .andExpect(forwardedUrl(GUEST_VIEW_PAGE));
      
	}
	
	@Test(expected=Test.None.class)
	public void testFetchGuestEntries() throws Exception {
		when(guestBookService.getImageById(1l)).thenReturn(blob); 
		byte[] bytes= new byte[10];
		when(blob.getBytes(1, (int) blob.length())).thenReturn(bytes);
        mockMvc.perform(get("/fetchGuestEntries?status=NEW&pageNo=1")) 
                 .andExpect(forwardedUrl(ADMIN_VIEW_PAGE));
	}

	
	@Test(expected=Test.None.class)
	public void testUpdateGuestEntry() throws Exception {
		 Mockito.doNothing().when(guestBookService).updateGuests(new ArrayList<GuestVo>());
	        mockMvc.perform(post("/updateGuestEntry").requestAttr("guest", guest))
	                 .andExpect(forwardedUrl("adminview"));
	}

	@Test(expected=Test.None.class)
	public void testGetGuestNoteImage() throws Exception {
		when(guestBookService.getImageById(1l)).thenReturn(blob); 
		byte[] bytes= new byte[10];
		when(blob.getBytes(1, (int) blob.length())).thenReturn(bytes);
        mockMvc.perform(get("/getGuestNoteImage?type=image&id=1")) //.requestAttr("id", "1").requestAttr("type", "image/jpg")
                 .andExpect(forwardedUrl(null));
	}
	@Test(expected=Test.None.class)
	public void testGetGuestNoteImageNull() throws Exception {
		when(guestBookService.getImageById(1l)).thenReturn(null); 
		byte[] bytes= new byte[10];
		when(blob.getBytes(1, (int) blob.length())).thenReturn(bytes);
        mockMvc.perform(get("/getGuestNoteImage?type=image/jpg&id=1")) //.requestAttr("id", "1").requestAttr("type", "image/jpg")
                 .andExpect(forwardedUrl(null));
	}

}
