package com.guestbook.controller;

import static com.guestbook.constants.GuestBookConstants.ADMIN_VIEW_PAGE;
import static com.guestbook.constants.GuestBookConstants.GUEST_LIST_FAIL_MSG;
import static com.guestbook.constants.GuestBookConstants.GUEST_LIST_SUCCESS_MSG;
import static com.guestbook.constants.GuestBookConstants.GUEST_SAVE_FAIL_MSG;
import static com.guestbook.constants.GuestBookConstants.GUEST_SAVE_SUCCESS_MSG;
import static com.guestbook.constants.GuestBookConstants.GUEST_VIEW_PAGE;
import static com.guestbook.constants.GuestBookConstants.LOGIN_FAIL_MSG;
import static com.guestbook.constants.GuestBookConstants.LOGIN_PAGE;
import static com.guestbook.constants.GuestBookConstants.LOGOUT_MSG;
import static com.guestbook.constants.GuestBookConstants.UPLOAD_FAIL_MSG;
import static com.guestbook.constants.GuestBookConstants.UPLOAD_SUCCESS_MSG;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;

import java.lang.reflect.Method;
import java.util.Properties;

import javax.crypto.BadPaddingException;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.mock.web.MockCookie;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.ModelAndView;

import com.guestbook.constants.UserRole;
import com.guestbook.service.GuestBookAuthService;
import com.guestbook.service.GuestBookService;
import com.guestbook.vo.GuestListVo;
import com.guestbook.vo.LoginVo;;

@RunWith(MockitoJUnitRunner.class)
@WebAppConfiguration
public class LoginControllerTest {
	
	@InjectMocks
	LoginController loginController;
	@Mock
	public  GuestBookAuthService guestBookAuthService;
	@Mock
	public  GuestBookService guestBookService;
	
	private MockMvc mockMvc;
	 @Spy
	 ResourceBundleMessageSource messageSource ;
	 @Spy
	 LoginVo login ;
	
	 @Mock
	 HttpServletRequest request;
	 @Mock
	 GuestListVo guestListVo;
	 MockHttpSession session = new MockHttpSession();
	 @Mock
	 MockCookie cookie;
	 
	@Before
	public void setUp() throws NoSuchMethodException, SecurityException  {
		System.setProperty("log4j.configurationFile","log4j2-testConfig.xml");
		 this.mockMvc = MockMvcBuilders.standaloneSetup( loginController).build();
		 Properties commonMessages = new Properties();
			messageSource.setCommonMessages(commonMessages);
			commonMessages.setProperty(UPLOAD_SUCCESS_MSG ,"File uploaded successfully");
			commonMessages.setProperty(UPLOAD_SUCCESS_MSG ,"File uploaded successfully");
			commonMessages.setProperty(UPLOAD_FAIL_MSG ,"File upload failed");
			commonMessages.setProperty(GUEST_SAVE_SUCCESS_MSG , "Guest entry saved successfully");
			commonMessages.setProperty(GUEST_SAVE_FAIL_MSG ,"Guest entry could no be saved");
			commonMessages.setProperty(GUEST_LIST_SUCCESS_MSG,"Records updated successfully");
			commonMessages.setProperty(GUEST_LIST_FAIL_MSG , "Guest entry saved successfully");
			commonMessages.setProperty(	LOGIN_FAIL_MSG, "login failed");
			commonMessages.setProperty(	LOGOUT_MSG, "logout successful");
			
	}

	@Test(expected=Test.None.class)
	public void testShowLogin() throws Exception {
        mockMvc.perform(get("/login"))
                 .andExpect(forwardedUrl(LOGIN_PAGE));
	}

	@Test(expected=Test.None.class)
	public void testShowDafultPage() throws Exception {
		  mockMvc.perform(get("/"))
          .andExpect(forwardedUrl(LOGIN_PAGE));
	}

	@Test
	public void testLogoutProcess() throws Exception {
		 mockMvc.perform(get("/logoutProcess").session(session).cookie(cookie))
         .andExpect(forwardedUrl("logoutview"));
	}
	
	@Test
	public void testLogoutProcessSessinNull() throws Exception {
		 mockMvc.perform(get("/logoutProcess").cookie(cookie))
         .andExpect(forwardedUrl("logoutview"));
	}

	//@Test(expected=Test.None.class)
	public void testLoginErrorProcess() throws Exception {
		 mockMvc.perform(get("/login?error"))
         .andExpect(forwardedUrl(LOGIN_PAGE));
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected=Test.None.class)
	public void testLoginProcessAuthError() throws Exception {
		when(guestBookAuthService.authenticateAndStartSession(login, request)).thenThrow(BadPaddingException.class);
		 mockMvc.perform(post("/loginProcess").requestAttr("login",login))
       .andExpect(forwardedUrl(LOGIN_PAGE));
	}

	@Test(expected=Test.None.class)
	public void testLoginProcessFailed() throws Exception {
		when(guestBookAuthService.authenticateAndStartSession(login, request)).thenReturn(UserRole.GUEST);
		 mockMvc.perform(post("/loginProcess").requestAttr("login",login))
        .andExpect(forwardedUrl(LOGIN_PAGE));
	}
	
	@Test(expected=Test.None.class)
	public void testPrepareDefaultGuestView() throws Exception {
		when(login.getUsername()).thenReturn("guest1");
		Method prepareDefaultGuestMethod = loginController.getClass().getDeclaredMethod("prepareDefaultGuestView",LoginVo.class);
		prepareDefaultGuestMethod.setAccessible(true);
		ModelAndView view = (ModelAndView) prepareDefaultGuestMethod.invoke(loginController, login);
		prepareDefaultGuestMethod.setAccessible(false);
		assertEquals(GUEST_VIEW_PAGE, view.getViewName());
	}
	@Test(expected=Test.None.class)
	public void testPrepareDefaultAdminView() throws Exception {
		when(login.getUsername()).thenReturn("admin1");
		when(guestBookService.getDefaultGuestListVo()).thenReturn(guestListVo);
		Method prepareDefaultAdminMethod = loginController.getClass().getDeclaredMethod("prepareDefaultAdminView",LoginVo.class);
		prepareDefaultAdminMethod.setAccessible(true);
		ModelAndView view = (ModelAndView) prepareDefaultAdminMethod.invoke(loginController, login);
		prepareDefaultAdminMethod.setAccessible(false);
		assertEquals(ADMIN_VIEW_PAGE, view.getViewName());
	}
}
