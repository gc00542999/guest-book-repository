package com.guestbook.controller;

import static com.guestbook.constants.GuestBookConstants.ERROR_ACCESS_DENIED_MSG;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;

import java.util.Properties;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.mock.web.MockCookie;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(MockitoJUnitRunner.class)
@WebAppConfiguration
public class ErrorControllerTest {

	@InjectMocks
	ErrorController errorController;

	private MockMvc mockMvc;

	@Spy
	ResourceBundleMessageSource messageSource;
	
	
	 MockHttpSession session = new MockHttpSession();
	 @Mock
	 MockCookie cookie;

	@Before
	public void setUp() {
		System.setProperty("log4j.configurationFile", "log4j2-testConfig.xml");
		this.mockMvc = MockMvcBuilders.standaloneSetup(errorController).build();
		Properties commonMessages = new Properties();
		messageSource.setCommonMessages(commonMessages);
		commonMessages.setProperty(ERROR_ACCESS_DENIED_MSG, "Access denied");
	

	}

	@Test
	public void testProcessDeniedAccessError() throws Exception {

		mockMvc.perform(get("/deniedAccess").session(session).cookie(cookie)).andExpect(forwardedUrl(null));

	}

}
