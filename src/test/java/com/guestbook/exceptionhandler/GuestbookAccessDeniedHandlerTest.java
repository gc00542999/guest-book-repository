package com.guestbook.exceptionhandler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class GuestbookAccessDeniedHandlerTest {

	@InjectMocks
	GuestbookAccessDeniedHandler guestbookAccessDeniedHandler;
	 @Mock
	 HttpServletRequest request;
	 @Mock
	 HttpServletResponse response;
	
	@Test(expected=Test.None.class)
	public void testHandle() throws IOException, ServletException {
		guestbookAccessDeniedHandler.setErrorPage("/deniedAccess");
		guestbookAccessDeniedHandler.handle(request, response, null);
	}

}
