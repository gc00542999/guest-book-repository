package com.guestbook.util;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.guestbook.dao.GuestDao;

@RunWith(MockitoJUnitRunner.class)
public class EncryptionUtilTest {

	@InjectMocks
	EncryptionUtil encryptionUtil;

	@Mock
	public GuestDao guestDao;
	
		
	@Test
	public void testEncryptAndDecrypt() throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
		String strToEncrypt = "test";
		String secret = "testKey";
		
		String encryptedValue = encryptionUtil.encrypt(strToEncrypt, secret);
		assertEquals(strToEncrypt, encryptionUtil.decrypt(encryptedValue, secret));
	}

	
	@Test
	public void testFetchSecretKeyFromValt() {
		String key = encryptionUtil.fetchSecretKeyFromValt();
		assertNotNull(key);
	}

}
