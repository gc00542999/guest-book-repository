package com.guestbook.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

/**
 * Domain class for data protection key
 * @author gautamc
 *
 */
@Getter
@Setter
@Entity
@Table (name = "data_protection_key")
public class DataProtectionKey implements Serializable {
	
	private static final long serialVersionUID = -1891922085655167163L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	@Column(length=50,nullable=false)
	private String name;
	@Column(length=30,nullable=true)
	private String type;
	@Column(length=600,nullable=false)
	private String value;

	
}
