package com.guestbook.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

/**
 * Domain class for user account
 * @author gautamc
 *
 */
@Getter
@Setter
@Entity(name = "user_account")
public class UserAccount {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private String username;
	private String password;
	private String role;
	private String firstname;
	private String lastname;
	private String email;
	private String address;
	private String phone;

	
}
