package com.guestbook.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.guestbook.domain.DataProtectionKey;

/**
 * Dao class for DataProtectionKey
 * @author gautamc
 *
 */
@Repository
public interface DataProtectionKeyDao  extends JpaRepository<DataProtectionKey,Long>{
	
	/**
	 * finds DataProtectionKey By Name And Type
	 * @param name String
	 * @param type String
	 * @return Data Protection Key DataProtectionKey
	 */
	DataProtectionKey findByNameAndType(String name,String type);
}
