package com.guestbook.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.guestbook.domain.UserAccount;
/**
 * Dao class for user account
 * @author gautamc
 *
 */
@Repository
public interface UserDao  extends JpaRepository<UserAccount,Long>{

	/**
	 * finds user account By User name
	 * @param username String
	 * @return User Account UserAccount
	 */
	UserAccount findByUsername(String username);
	  
}
