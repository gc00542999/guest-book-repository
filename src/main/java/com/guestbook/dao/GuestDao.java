package com.guestbook.dao;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.guestbook.domain.Guest;

/**
 * Doa class for Guest
 * @author gautamc
 *
 */
@Repository
public interface GuestDao  extends JpaRepository<Guest,Long>{
	
	/**
	 * find By Status
	 * @param status String
	 * @return guest Guest
	 */
	List<Guest> findByStatus(String status);
	
	/**
	 * 
	 * @param status
	 * @param pageable Pageable
	 * @return guest Guest
	 */
	List<Guest> findByStatus(String status, Pageable pageable);
	
	/**
	 * counts By Status
	 * @param status String
	 * @return guest Guest
	 */
	long countByStatus(String status);
}
