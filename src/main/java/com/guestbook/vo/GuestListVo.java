package com.guestbook.vo;

import java.io.Serializable;
import java.util.List;

import com.guestbook.constants.Status;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class GuestListVo implements Serializable{
	private static final long serialVersionUID = -2259200579421807903L;
	
	private List<GuestVo> guests;
	private String username;
	private int pageNo;
	private Status status;
	private int totalPages;

}
