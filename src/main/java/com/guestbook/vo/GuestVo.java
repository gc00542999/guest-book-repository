package com.guestbook.vo;

import java.io.Serializable;
import java.sql.Blob;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class GuestVo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7933930401583691294L;

	private Long id;

	private String note;

	
	private transient Blob image;
	
	private String  username;
	private String status;
	private String type;
	
	
}
