package com.guestbook.service;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.guestbook.constants.Status;
import com.guestbook.domain.Guest;
import com.guestbook.vo.GuestListVo;
import com.guestbook.vo.GuestVo;
/**
 * Service class for Guest book
 * @author gautamc
 *
 */
public interface GuestBookService {
	/**
	 * adds Guest
	 * @param guest GuestVo
	 * @return guest Guest
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 */
	Guest addGuest(GuestVo guest) throws IllegalAccessException, InvocationTargetException;

	/**
	 * updates Guests
	 * @param usernames String
	 */
	void updateGuests(List<GuestVo> usernames);

	/**
	 * gets Image By Id
	 * @param id Long
	 * @return image Blob
	 */
	Blob getImageById(Long id);

	/**
	 * gets All New Guest Entries
	 * @param pageNo int
	 * @param status Status
	 * @return guestVos List<GuestVo>
	 */
	List<GuestVo> getAllNewGuestEntries(int pageNo,Status status);

	/**
	 * 
	 * @param file
	 * @param username
	 * @throws IOException
	 * @throws SQLException
	 */
	void addGuestWithImage(MultipartFile file, String username) throws IOException, SQLException;
	
	/**
	 * gets Total Guest Pages
	 * @param status Status
	 * @return total pages int
	 */
	int getTotalGuestPages(Status status);

	/**
	 * gets Default Guest List Vo
	 * @return guestListVo GuestListVo
	 */
	GuestListVo getDefaultGuestListVo();
}
