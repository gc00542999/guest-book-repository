package com.guestbook.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.guestbook.dao.UserDao;
import com.guestbook.domain.UserAccount;

/**
 * Custom implementation for User details service
 * @author gautamc
 *
 */
public class UserDetailsServiceImpl implements UserDetailsService {
	private static final Logger LOGGER = LoggerFactory.getLogger(UserDetailsServiceImpl.class);
	
	@Autowired
	private UserDao  userDao;
	
	
	@Override
	public UserDetails loadUserByUsername(String username) {
		LOGGER.info("LoadUserByUsername Starts for usename: {}", username);
		UserAccount userAccount=userDao.findByUsername(username);
				if (userAccount == null) {
	                throw new UsernameNotFoundException(new StringBuilder("No user found with username: ").append(username).toString());
	            }
		 User.UserBuilder users = User.builder();
		UserDetails userDetails =users.username(username).password(userAccount.getPassword()).roles(userAccount.getRole()).build();
		LOGGER.info("LoadUserByUsername ends for usename: {}", username);
		
		return userDetails;
		
	}
	

}
