package com.guestbook.service;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.http.HttpServletRequest;

import com.guestbook.constants.UserRole;
import com.guestbook.vo.LoginVo;

/**
 * Service class for Guest book Authentication
 * @author gautamc
 *
 */
public interface GuestBookAuthService {

	/**
	 * authenticates And Starts Session
	 * @param login LoginVo
	 * @param request request
	 * @return User Role UserRole
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws InvalidAlgorithmParameterException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 */
	UserRole authenticateAndStartSession(LoginVo login, HttpServletRequest request)
			throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException;

	/**
	 * gets User name
	 * @return User name String
	 */
	String getUsername();
}
