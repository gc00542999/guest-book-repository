package com.guestbook.service;

import static com.guestbook.constants.GuestBookConstants.FILE_NAME_HEADER;
import static com.guestbook.constants.GuestBookConstants.PAGE_LIMIT;
import static com.guestbook.constants.GuestBookConstants.REGEX_FILE_EXTENSION_SEPARATOR;
import static com.guestbook.constants.GuestBookConstants.REGEX_FILE_PATH_SEPARATOR;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.sql.rowset.serial.SerialBlob;
import javax.transaction.Transactional;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.guestbook.constants.Status;
import com.guestbook.dao.GuestDao;
import com.guestbook.domain.Guest;
import com.guestbook.vo.GuestListVo;
import com.guestbook.vo.GuestVo;

/**
 * 
 * Service implementation class for Guest book
 * @author gautamc
 *
 */
@Service
public class GuestBookServiceImpl implements GuestBookService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(GuestBookServiceImpl.class);

	@Autowired
	private GuestDao guestDao;
	@Autowired
	private GuestBookAuthService guestBookAuthService;

	@Override
	@Transactional
	public Guest addGuest(GuestVo guestVo) throws IllegalAccessException, InvocationTargetException {
		Guest guest = new Guest();
		BeanUtils.copyProperties(guest, guestVo);

		guest.setStatus(Status.NEW.name());
		return guestDao.save(guest);

	}

	@Override
	@Transactional
	public void updateGuests(List<GuestVo> guestVos) {
		List<Guest> guests = guestVos.stream().map((GuestVo guestVo) -> {
			Guest guest = new Guest();
			try {
				BeanUtils.copyProperties(guest, guestVo);
			} catch (IllegalAccessException | InvocationTargetException e) {
				LOGGER.equals(e.fillInStackTrace());
			}
			return guest;
		}).collect(Collectors.toList());
		guestDao.saveAll(guests);
	}

	@Override
	public List<GuestVo> getAllNewGuestEntries(int pageNo, Status status) {
		Pageable pageable = PageRequest.of(pageNo - 1, PAGE_LIMIT, Sort.by("id"));
		 List<Guest> guests = guestDao.findByStatus(status.name(), pageable);
		return guests.stream().map((Guest guest) -> {
			
			GuestVo guestVo = new GuestVo();
			try {
				BeanUtils.copyProperties(guestVo, guest);
			} catch (IllegalAccessException | InvocationTargetException e) {
				LOGGER.equals(e.fillInStackTrace());
			}
			return guestVo;
		}).collect(Collectors.toList());
	}

	@Override
	public Blob getImageById(Long id) {
		Optional<Guest> guest = guestDao.findById(id);
		return guest.isPresent() ? guest.get().getImage() : null;

	}

	@Override
	public void addGuestWithImage(MultipartFile file, String username) throws IOException, SQLException {
		Guest guest = new Guest();

		byte[] bytes = file.getBytes();
		guest.setStatus(Status.NEW.name());
		guest.setImage(new SerialBlob(bytes));
		guest.setUsername(username);
		guest.setNote(getFileNote(file.getOriginalFilename()));
		guest.setType(file.getContentType());
		guestDao.save(guest);

	}

	/**
	 * Gets File notes
	 * @param originalFileName String
	 * @return fileNote String
	 */
	private String getFileNote(String originalFileName) {
		String[] nameParts = originalFileName.split(REGEX_FILE_PATH_SEPARATOR);
		String[] fileName = nameParts[nameParts.length - 1].split(REGEX_FILE_EXTENSION_SEPARATOR);
		return new StringBuilder(FILE_NAME_HEADER).append(fileName[0]).toString();
	}

	@Override
	public int getTotalGuestPages(Status status) {
		long count = guestDao.countByStatus(status.name());
		int totalPages = 0;
		if (count == 0 || count % PAGE_LIMIT == 0) {
			totalPages = (int) (count / PAGE_LIMIT);
		} else {
			totalPages = (int) (count / PAGE_LIMIT + 1);
		}

		return totalPages;
	}

	@Override
	public GuestListVo getDefaultGuestListVo() {

		
		return new GuestListVo(getAllNewGuestEntries(1, Status.NEW), guestBookAuthService.getUsername(), 1, Status.NEW,
				getTotalGuestPages(Status.NEW));
	}

}
