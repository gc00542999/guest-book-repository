package com.guestbook.service;

import static com.guestbook.constants.GuestBookConstants.USER_ROLE_TOKEN;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.guestbook.constants.DataProtectionKeyType;
import com.guestbook.constants.GuestBookConstants;
import com.guestbook.constants.UserRole;
import com.guestbook.dao.DataProtectionKeyDao;
import com.guestbook.domain.DataProtectionKey;
import com.guestbook.util.EncryptionUtil;
import com.guestbook.vo.LoginVo;

/**
 * Service implementation class for Guest book Authentication
 * @author gautamc
 *
 */
@Service
public class GuestBookAuthServiceImpl implements GuestBookAuthService {

	private static final Logger LOGGER = LoggerFactory.getLogger(GuestBookAuthServiceImpl.class);

	@Autowired
	private AuthenticationManager authenticationManager;
	@Autowired
	private DataProtectionKeyDao dataProtectionKeyDao;
	@Autowired
	private EncryptionUtil encryptionUtil;

	@Override
	public UserRole authenticateAndStartSession(LoginVo login, HttpServletRequest request) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
		UserRole role = null;
		if (null != login) {
			UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
					login.getUsername(), new StringBuilder(login.getPassword()).append( getSalt()).toString());

			Authentication authentication = authenticationManager.authenticate(authenticationToken);

			SecurityContext securityContext = SecurityContextHolder.getContext();
			securityContext.setAuthentication(authentication);
			request.getSession(true).setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY,
					securityContext);
			role = getRole(authentication);

		}
		LOGGER.info("Role is {}",role);
		return role;
	}

	/**
	 * Gets Salt for password encryption
	 * @return salt String
	 * @throws BadPaddingException 
	 * @throws IllegalBlockSizeException 
	 * @throws InvalidAlgorithmParameterException 
	 * @throws NoSuchPaddingException 
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidKeyException 
	 */
	private String getSalt() throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
		DataProtectionKey dataProtectionKey = dataProtectionKeyDao.findByNameAndType(GuestBookConstants.SALT_LOGIN,
				DataProtectionKeyType.SALT.name());

		return encryptionUtil.decrypt(dataProtectionKey.getValue(), encryptionUtil.fetchSecretKeyFromValt());

	}

	/**
	 * Gets Role corresponding to input authentication
	 * @param authentication Authentication
	 * @return userRole UserRole
	 */
	private UserRole getRole(Authentication authentication) {
		UserDetails user = (UserDetails) authentication.getPrincipal();
		Collection<? extends GrantedAuthority> authorities = user.getAuthorities();
		UserRole role = null;
		for (GrantedAuthority authority : authorities) {
			if (null != authority && !StringUtils.isEmpty(authority.getAuthority())
					&& authority.getAuthority().contains(USER_ROLE_TOKEN)) {

				role = UserRole.valueOf(authority.getAuthority().split(USER_ROLE_TOKEN)[1]);
				break;
			}

		}
		return role;
	}

	@Override
	public String getUsername() {
		String username = null;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (principal instanceof UserDetails) {
			username = ((UserDetails) principal).getUsername();
		} else {
			username = principal.toString();
		}
		return username;
	}

}
