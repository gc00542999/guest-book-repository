package com.guestbook.util;

import static com.guestbook.constants.GuestBookConstants.TRANSFORMATION_MODE;

import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.stereotype.Component;

/**
 * Encryption utility class
 * @author gautamc
 *
 */
@Component
public class EncryptionUtil {

	/** In production System, It will be fetched from Valt (HSM) **/
	private static final String SECRET_KEY = "secretkey"; 

	public static final int AES_KEY_SIZE = 128; /** in bits**/
	public static final int GCM_NONCE_LENGTH = 12; /** in bytes **/
	public static final int GCM_TAG_LENGTH = 16; /**  in bytes  **/

	/**
	 * Encrypts key
	 * 
	 * @param strToEncrypt
	 *            String
	 * @param secret
	 *            String
	 * @return encrypted value String
	 * @throws InvalidAlgorithmParameterException
	 * @throws NoSuchPaddingException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 * @throws BadPaddingException
	 * @throws IllegalBlockSizeException
	 */
	public String encrypt(String strToEncrypt, String secret) throws InvalidKeyException, NoSuchAlgorithmException,
			NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
		Cipher cipher = getCipher(secret, Cipher.ENCRYPT_MODE);

		return Base64.getEncoder().encodeToString(cipher.doFinal(strToEncrypt.getBytes(StandardCharsets.UTF_8)));

	}

	/**
	 * Decrypts key
	 * 
	 * @param strToDecrypt
	 *            String
	 * @param secret
	 *            String
	 * @return decrypted value String
	 * @throws InvalidAlgorithmParameterException
	 * @throws NoSuchPaddingException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 * @throws BadPaddingException
	 * @throws IllegalBlockSizeException
	 */
	public String decrypt(String strToDecrypt, String secret) throws InvalidKeyException, NoSuchAlgorithmException,
			NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {

		Cipher cipher = getCipher(secret, Cipher.DECRYPT_MODE);

		return new String(cipher.doFinal(Base64.getDecoder().decode(strToDecrypt)));

	}

	/**
	 * sets key into secret key specification
	 * 
	 * @param vaultKey
	 *            String
	 * @return secret key specification SecretKeySpec
	 * @throws NoSuchAlgorithmException 
	 */
	private SecretKeySpec setKey(String vaultKey) throws NoSuchAlgorithmException {
		MessageDigest messageDigest = null;
		byte[] key = null;
		SecretKeySpec secretKey = null;

		key = vaultKey.getBytes(StandardCharsets.UTF_8);

		messageDigest = MessageDigest.getInstance("SHA-1");

		key = messageDigest.digest(key);

		key = Arrays.copyOf(key, 16);
		secretKey = new SecretKeySpec(key, "AES");

		return secretKey;
	}

	/**
	 * Gets Cipher
	 * 
	 * @param secret
	 *            String
	 * @param cipherMode
	 *            String
	 * @return cipher Cipher
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws InvalidKeyException
	 * @throws InvalidAlgorithmParameterException
	 */
	private Cipher getCipher(String secret, int cipherMode) throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, InvalidAlgorithmParameterException {
		SecretKeySpec secretKey = setKey(secret);
		Cipher cipher = Cipher.getInstance(TRANSFORMATION_MODE);
		final byte[] nonce = new byte[GCM_NONCE_LENGTH];
		GCMParameterSpec paramSpec = new GCMParameterSpec(GCM_TAG_LENGTH * 8, nonce);
		cipher.init(cipherMode, secretKey, paramSpec);
		return cipher;

	}

	/**
	 * This should fetch the secret key from a secured vault in production environment.
	 * For development purpose, current implementation fetches it from a constant string
	 * @return secret key String
	 */
	public String fetchSecretKeyFromValt() {
		return SECRET_KEY;
	}

}