package com.guestbook.constants;

/**
 * Enum of user roles
 * @author gautamc
 *
 */
public enum UserRole {
GUEST,ADMIN;

}
