package com.guestbook.constants;

/**
 * This class holds all the constants for guest book application
 * @author gautamc
 *
 */
public class GuestBookConstants {
	
	public static final String USER_ROLE_TOKEN = "ROLE_";
	public static final int PAGE_LIMIT = 2;

	public static final String SALT_LOGIN = "SALT_LOGIN";

	public static final String TRANSFORMATION_MODE = "AES/GCM/NoPadding";
	public static final String CONTENT_TYPE_IMAGE = "image";
	public static final String CONTENT_TYPE_SEPARATOR = "/";
	
	/* message bundle constants */
	public static final String LOGOUT_MSG = "logout.msg";
	public static final String UPLOAD_SUCCESS_MSG = "upload.success.msg";
	public static final String UPLOAD_FAIL_MSG = "upload.fail.msg";
	public static final String GUEST_SAVE_SUCCESS_MSG = "guest.save.success.msg";
	public static final String GUEST_SAVE_FAIL_MSG = "guest.save.fail.msg";
	public static final String GUEST_LIST_SUCCESS_MSG = "guestlist.update.success.msg";
	public static final String GUEST_LIST_FAIL_MSG = "guestlist.update.fail.msg";
	public static final String LOGIN_FAIL_MSG = "login.fail.msg";
	public static final String ERROR_GENERIC_MSG = "error.generic.msg";
	public static final String ERROR_ACCESS_DENIED_MSG = "error.access.denied.msG";
	
	
	/*message constants */
	public static final String ERROR_MSG = "errorMsg";
	public static final String SUCCESS_MSG = "successMsg";
	public static final String ERROR_PAGE = "errorpage";
	public static final String LOGIN_PAGE = "loginview";
	public static final String LOGOUT_PAGE = "logoutview";
	public static final String GUEST_VIEW_PAGE = "guestview";
	public static final String ADMIN_VIEW_PAGE = "adminview";

	public static final String REGEX_FILE_PATH_SEPARATOR = "\\\\";
	public static final String REGEX_FILE_EXTENSION_SEPARATOR = "\\.";
	public static final String FILE_NAME_HEADER = "File name: ";


	private GuestBookConstants() {

	}

}
