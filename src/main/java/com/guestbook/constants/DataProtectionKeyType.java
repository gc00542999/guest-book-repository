package com.guestbook.constants;

/**
 * Enum of data protection key types
 * @author gautamc
 *
 */
public enum DataProtectionKeyType {
	ENCRYPTION_KEY,SALT;
}
