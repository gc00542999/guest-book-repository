package com.guestbook.constants;

/**
 * Enum of Statuses 
 * @author gautamc
 *
 */
public enum Status {
APPROVED,DELETED,NEW;
}
