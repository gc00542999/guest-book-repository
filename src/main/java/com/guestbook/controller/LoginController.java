package com.guestbook.controller;

import static com.guestbook.constants.GuestBookConstants.ADMIN_VIEW_PAGE;
import static com.guestbook.constants.GuestBookConstants.GUEST_VIEW_PAGE;
import static com.guestbook.constants.GuestBookConstants.LOGIN_FAIL_MSG;
import static com.guestbook.constants.GuestBookConstants.LOGIN_PAGE;
import static com.guestbook.constants.GuestBookConstants.LOGOUT_MSG;
import static com.guestbook.constants.GuestBookConstants.SUCCESS_MSG;
import static com.guestbook.constants.GuestBookConstants.ERROR_MSG;
import static com.guestbook.constants.GuestBookConstants.LOGOUT_PAGE;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.guestbook.constants.UserRole;
import com.guestbook.domain.Guest;
import com.guestbook.service.GuestBookAuthService;
import com.guestbook.service.GuestBookService;
import com.guestbook.vo.LoginVo;

/**
 * Login Controller class
 * @author gautamc
 *
 */
@Controller
public class LoginController {
	private static final Logger LOGGER = LoggerFactory.getLogger(LoginController.class);
	
	private static final String	VIEW_OBJ_GUEST="guest";
	@Autowired
	private GuestBookService guestBookService;
	@Autowired
	private ResourceBundleMessageSource messageSource;
	@Autowired
	private GuestBookAuthService guestBookAuthService;

	/**
	 * show Login page
	 * @param request
	 * @param response
	 * @return ModelAndView
	 */
	@GetMapping(value = "/login")
	public ModelAndView showLogin(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView view = new ModelAndView(LOGIN_PAGE);
		view.addObject("login", new LoginVo());
		return view;
			
		
	}
	/**
	 * shows Default Page
	 * @param request
	 * @param response
	 * @return
	 */
	@GetMapping(value = "/")
	public ModelAndView showDefaultPage(HttpServletRequest request, HttpServletResponse response) {
		return showLogin(request, response);
	}

	/**
	 * Process logout
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException 
	 */
	@GetMapping(value = "/logoutProcess")
	public ModelAndView processLogout(HttpServletRequest request, HttpServletResponse response) {
		
		SecurityContextHolder.clearContext();
        HttpSession session= request.getSession(false);
       if(session != null) {
           session.invalidate();
       }
       for(Cookie cookie : request.getCookies()) {
           cookie.setMaxAge(0);
       }
       ModelAndView view = new ModelAndView(LOGOUT_PAGE);
		view.addObject(SUCCESS_MSG, messageSource.getMessage(LOGOUT_MSG, null, LocaleContextHolder.getLocale()));
		return view;
 	
	}

	
	/**
	 * Process login
	 * @param request
	 * @param response
	 * @param login LoginVo
	 * @return ModelAndView
	 */
	@PostMapping(value = "/loginProcess")
	public ModelAndView processLogin(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("login") LoginVo login) {
		ModelAndView view = null;
		UserRole role =null;
		try {
			role = guestBookAuthService.authenticateAndStartSession(login, request);
		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException
				| InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
			LOGGER.error("exception occuered during authentication for user :{}", login.getUsername());
		}catch(BadCredentialsException e){
			LOGGER.error("Invalid username or password.");
				
		}

		if (null != role && UserRole.GUEST.equals(role)) {
			view = prepareDefaultGuestView(login);
		} else if (null != role && UserRole.ADMIN.equals(role)) {
			
			view = prepareDefaultAdminView(login);
		} else {
			view = new ModelAndView(LOGIN_PAGE);
			view.addObject(ERROR_MSG, messageSource.getMessage(LOGIN_FAIL_MSG, null, LocaleContextHolder.getLocale()));

			LOGGER.info("login Failed");
		}
		return view;
	}

	private ModelAndView prepareDefaultGuestView(LoginVo login){
		ModelAndView view = new ModelAndView(GUEST_VIEW_PAGE);
		Guest guest = new Guest();
		guest.setUsername(login.getUsername());
		view.addObject(VIEW_OBJ_GUEST, guest);
		view.addObject("file", null);
		LOGGER.info("login Success for Guest user :{}", login.getUsername());
		return view;
	}
	
	private ModelAndView prepareDefaultAdminView(LoginVo login){
		ModelAndView view = new ModelAndView(ADMIN_VIEW_PAGE);
		view.addObject("guestListVo", guestBookService.getDefaultGuestListVo());
		
		LOGGER.info("login Success for Admin user :{}", login.getUsername());
		return view;
	}
}
