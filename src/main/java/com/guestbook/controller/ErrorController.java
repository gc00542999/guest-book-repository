package com.guestbook.controller;

import java.io.IOException;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Error controller class
 * @author gautamc
 *
 */
@Controller
public class ErrorController {
	
	public static final String LOGIN_URL="login";
	@Autowired
	ResourceBundleMessageSource messageSource;
	
	/**
	 * Process logout
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException 
	 */
	@GetMapping(value = "/deniedAccess")
	public void processDeniedAccessError(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		SecurityContextHolder.clearContext();
        HttpSession session= request.getSession(false);
       if(session != null) {
           session.invalidate();
       }
       for(Cookie cookie : request.getCookies()) {
           cookie.setMaxAge(0);
       }
   	response.sendRedirect(LOGIN_URL);
 	
	}

}
