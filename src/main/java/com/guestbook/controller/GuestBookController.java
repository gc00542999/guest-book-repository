package com.guestbook.controller;

import static com.guestbook.constants.GuestBookConstants.ADMIN_VIEW_PAGE;
import static com.guestbook.constants.GuestBookConstants.ERROR_MSG;
import static com.guestbook.constants.GuestBookConstants.GUEST_LIST_FAIL_MSG;
import static com.guestbook.constants.GuestBookConstants.GUEST_LIST_SUCCESS_MSG;
import static com.guestbook.constants.GuestBookConstants.GUEST_SAVE_FAIL_MSG;
import static com.guestbook.constants.GuestBookConstants.GUEST_SAVE_SUCCESS_MSG;
import static com.guestbook.constants.GuestBookConstants.GUEST_VIEW_PAGE;
import static com.guestbook.constants.GuestBookConstants.SUCCESS_MSG;
import static com.guestbook.constants.GuestBookConstants.UPLOAD_FAIL_MSG;
import static com.guestbook.constants.GuestBookConstants.UPLOAD_SUCCESS_MSG;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.guestbook.constants.Status;
import com.guestbook.domain.Guest;
import com.guestbook.service.GuestBookAuthService;
import com.guestbook.service.GuestBookService;
import com.guestbook.vo.GuestListVo;
import com.guestbook.vo.GuestVo;

/**
 * guest book controller class
 * @author gautamc
 *
 */
@Controller
public class GuestBookController {
	private static final Logger LOGGER = LoggerFactory.getLogger(GuestBookController.class);
	@Autowired
	private GuestBookService guestBookService;
	@Autowired
	private ResourceBundleMessageSource messageSource;
	
	@Autowired
	private GuestBookAuthService guestBookAuthService;

	

	@PostMapping(value = "/addGuestEntry")
	public ModelAndView addGuestEntry(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("guest") GuestVo guest) {
		String errorMsg = null;
		String successMsg = null;
		try {
			guestBookService.addGuest(guest);
			successMsg = messageSource.getMessage(GUEST_SAVE_SUCCESS_MSG, null, LocaleContextHolder.getLocale());
		} catch (Exception e) {
			errorMsg = messageSource.getMessage(GUEST_SAVE_FAIL_MSG, null, LocaleContextHolder.getLocale());
		}
		return createGuestEntryView(successMsg, errorMsg);
	}
	
	

	@PostMapping(value = "/uploadFile")
	public ModelAndView uploadFile(@RequestParam("file") MultipartFile file) {
		String errorMsg = null;
		String successMsg = null;
		try {
			guestBookService.addGuestWithImage(file, guestBookAuthService.getUsername());
			successMsg = messageSource.getMessage(UPLOAD_SUCCESS_MSG, null, LocaleContextHolder.getLocale());
		} catch (Exception e) {
			errorMsg = messageSource.getMessage(UPLOAD_FAIL_MSG, null, LocaleContextHolder.getLocale());
			LOGGER.error("Upload Failed");
		}

		return createGuestEntryView(successMsg, errorMsg);
	}

	private ModelAndView createGuestEntryView(String successMsg, String errorMsg) {
		ModelAndView view = new ModelAndView(GUEST_VIEW_PAGE);
		Guest guest = new Guest();
		guest.setUsername(guestBookAuthService.getUsername());
		return view.addObject("guest", guest).addObject("file", null)
				.addObject(ERROR_MSG, errorMsg).addObject(SUCCESS_MSG, successMsg);
	}

	@GetMapping(value = "/fetchGuestEntries")
	public ModelAndView fetchGuestEntries(HttpServletRequest request, HttpServletResponse response,
			@RequestParam("status") Status status,@RequestParam("pageNo") int pageNo) {
		String errorMsg = null;
		String successMsg = null;
		
			
			
			List<GuestVo> guests =  guestBookService.getAllNewGuestEntries(pageNo,status);
		
			int totalPages = guestBookService.getTotalGuestPages(status);
			ModelAndView view = new ModelAndView(ADMIN_VIEW_PAGE);
			return view.addObject("guestListVo", new GuestListVo(guests, guestBookAuthService.getUsername(), pageNo, status, totalPages ))
					.addObject(ERROR_MSG, errorMsg).addObject(SUCCESS_MSG, successMsg);
			
	}
	
	@PostMapping(value = "/updateGuestEntry")
	public ModelAndView updateGuestEntry(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("guestListVo") GuestListVo guestListVo) {
		String errorMsg = null;
		String successMsg = null;
		try {
			guestBookService.updateGuests(guestListVo.getGuests());
			successMsg = messageSource.getMessage(GUEST_LIST_SUCCESS_MSG, null, LocaleContextHolder.getLocale());
		} catch (Exception e) {
			errorMsg = messageSource.getMessage(GUEST_LIST_FAIL_MSG, null, LocaleContextHolder.getLocale());

		}
		guestListVo.setGuests(guestBookService.getAllNewGuestEntries(guestListVo.getPageNo(),guestListVo.getStatus()));
		guestListVo.setTotalPages(guestBookService.getTotalGuestPages(guestListVo.getStatus()));
		ModelAndView view = new ModelAndView(ADMIN_VIEW_PAGE);
		return view.addObject("guestListVo", guestListVo)
				.addObject(ERROR_MSG, errorMsg).addObject(SUCCESS_MSG, successMsg);
		
	}

	@GetMapping(value = "/getGuestNoteImage")
	public void getGuestNoteImage(HttpServletResponse response, @RequestParam("type") String type,  @RequestParam("id") long id) throws SQLException, IOException  {
		
		response.setContentType(type);

		Blob ph = guestBookService.getImageById(id);
		if (ph != null) {
			byte[] bytes = ph.getBytes(1, (int) ph.length());
			InputStream inputStream = new ByteArrayInputStream(bytes);
			IOUtils.copy(inputStream, response.getOutputStream());
		}
	}
	
	
	
	

}