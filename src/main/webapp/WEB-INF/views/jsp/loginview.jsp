<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<title>Login</title>
</head>
<body>
	<div class="container">
		<h2>Welcome ${guest.username}</h2>
		<div class="panel-group">
			<div class="panel panel-primary">
				<c:if test="${ not empty errorMsg}">
					<div class="alert alert-danger alert-dismissible fade in">
						${errorMsg}</div>
				</c:if>

				<c:if test="${ not empty successMsg}">
					<div class="alert alert-success alert-dismissible fade in">
						${successMsg}</div>
				</c:if>
			</div>
			<div class="panel panel-primary">
				<div class="panel-heading">Login Here</div>
				<div class="panel-body">
					<form:form id="loginForm" modelAttribute="login"
						action="loginProcess" method="post">
						<div class="form-group">
							<label for="usr">Name:</label> <input type="text"
								class="form-control" id="usr" name="username" required
								maxlength="45">
						</div>
						<div class="form-group">
							<label for="pwd">Password:</label> <input type="password"
								class="form-control" id="pwd" name="password" required
								maxlength="45">

						</div>
							<input type="hidden" name="${_csrf.parameterName}"
									value="${_csrf.token}" />
						<button type="submit" class="btn btn-primary">Login</button>
					</form:form>
				</div>
			</div>
		</div>
	</div>


</body>
</html>