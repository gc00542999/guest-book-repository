<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Guest Entry Book</title>

<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<script src="resources/core/js/guestbook.js"></script>


</head>


<body>


	<div class="container">
		<h2>Hello ${guest.username}</h2>
		<div class="panel-group">
			<div class="panel panel-primary">

				<div class="panel-body">
					<table style="width: 100%">
						<tr style="width: 100%">
							<td style="width: 90%; text-align: left">
							
								<div class="alert alert-danger alert-dismissible fade in"
									style="visibility: hidden;" id="divErrorMsg">
									<label id="errorMsg"> ${errorMsg} </label>
								</div> <c:if test="${ not empty errorMsg}">
									<div  id="divErrorServer" class="alert alert-danger alert-dismissible fade in">
										${errorMsg}</div>
								</c:if> <c:if test="${ not empty successMsg}">
									<div id="divSuccessServer" style="visibility: visible;" class="alert alert-success alert-dismissible fade in">
										${successMsg}</div>
								</c:if>
							</td>
							<td style="width: 10%; text-align: right"><form:form
									id="textForm" action="logoutProcess" method="get">
									<input id="logoutAlertMSG" type="hidden"
										value="<spring:message code='logout.alert.msg'/>" />

									<button type="submit" class="btn btn-default"
										onclick="return confirm($('#logoutAlertMSG').val())">Logout</button>
								</form:form></td>
						</tr>
					</table>
				</div>
			</div>
			<div class="panel panel-primary">
				<div class="panel-heading">Guest Notes</div>
				<div class="panel-body">

					<form:form id="textForm" modelAttribute="guest"
						action="addGuestEntry" method="post">

						<form:hidden path="username" />

						<div class="form-group">
							<label for="comment">Comment:</label>
							<textarea class="form-control" rows="5" id="comment" name="note"
								maxlength="250"></textarea>
						</div>
						<input id="noteSubmitMSG" type="hidden"
							value="<spring:message code='guestview.submit.details.alert.msg'/>" />
						<button type="submit" class="btn btn-primary"
							onclick="return confirm($('#noteSubmitMSG').val())">Submit</button>


					</form:form>
				</div>
			</div>

			<div class="panel panel-primary">
				<div class="panel-heading">Upload your file</div>
				<div class="panel-body">
					<input id="fileSubmitMSG" type="hidden"
							value="<spring:message code='guestview.submit.details.alert.msg'/>" />
	<input id="fileImageMSG" type="hidden"
							value="<spring:message code='image.size.max'/>" />
					<form method="POST" action="uploadFile"
						enctype="multipart/form-data"
						onsubmit="return validateImageFile('fileImage', $('#fileImageMSG').val())">
						<table style="text-align: center;">
							<tr>
								<input type="hidden" name="${_csrf.parameterName}"
									value="${_csrf.token}" />
								<input type="file" name="file" id="fileImage">
								</file>

							</tr>
							<tr>

								<button type="submit" class="btn btn-primary">Upload</button>
							</tr>

						</table>
					</form>
				</div>
			</div>


		</div>
	</div>








	 





</body>
</html>