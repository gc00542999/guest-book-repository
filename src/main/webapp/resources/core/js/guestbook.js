function validateImageFile(id, max) {
	var allowedExtension = [ 'jpeg', 'jpg', 'png', 'gif', 'bmp' ];
	var input = document.getElementById(id);
	var fileExtension = input.value.split('.').pop().toLowerCase();
	var isValidFile = false;

	for ( var index in allowedExtension) {

		if (fileExtension === allowedExtension[index]) {
			isValidFile = true;
			break;
		}
	}
	var test = input.files[0];
	
	if (!isValidFile) {
		document.getElementById('errorMsg').innerHTML = 'Allowed Extensions are : *.'
				+ allowedExtension.join(', *.');

		document.getElementById('divErrorMsg').style.visibility = "visible";

	} else if (test.size > max) {
		
		document.getElementById('errorMsg').innerHTML = 'The Image file exceeds its maximum permitted size of'
				+ max + ' bytes ';
		document.getElementById('divErrorMsg').style.visibility = "visible";
		isValidFile = false;
	} else {
		document.getElementById('divErrorMsg').style.visibility = "hidden";
		
	}

	if(document.getElementById('divErrorServer') !== null){
	document.getElementById('divErrorServer').style.visibility = "hidden";
	}
	if(document.getElementById('divSuccessServer') !== null){
	document.getElementById('divSuccessServer').style.visibility = "hidden";
	}
	
		return isValidFile;
	
}
